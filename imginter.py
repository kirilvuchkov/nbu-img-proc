from PIL import Image
import numpy
from images2gif import writeGif
import math
from scipy.interpolate import interp2d

def vec(t):
    return numpy.array([t[0], t[1], t[2]])

def adjust(img, size, color):
    sx = size[0]
    sy = size[1]

    hdif = img.size[1] - sy
    wdif = img.size[0] - sx

    top = hdif/2
    left = wdif/2

    imgData = img.getdata()
    result = list()

    for y in range(sy):
        for x in range(sx):
            ix = x + left
            iy = y + top
            if(ix < 0 or ix >= img.size[0] or iy < 0 or iy >= img.size[1]):
                result.append(color)
            else:
                result.append(imgData[ix + iy * img.size[0]])

    resImg = Image.new(img.mode, size)
    resImg.putdata(result)
    return resImg


def lin_interop(p1, p2, s):
    # sss = 0.2
    x = p1[0]*s + p2[0]*(1-s)
    y = p1[1]*s + p2[1]*(1-s)
    z = p1[2]*s + p2[2]*(1-s)
    return (int(x), int(y), int(z))

def image_interop(start, end, s):
    sx = (max(start.size[0], end.size[0]))
    sy = (max(start.size[1], end.size[1]))

    start = adjust(start, (sx, sy), (255, 255, 255))
    end = adjust(end, (sx, sy), (255, 255, 255))

    data1 = start.getdata()
    data2 = end.getdata()
    result = list()
    for i in range(len(data1)):
        result.append(lin_interop(data1[i], data2[i], s))
    resultImage = Image.new(start.mode, (sx, sy))
    resultImage.putdata(result)
    return resultImage

img1 = Image.open("/Users/kirilvuchkov/Downloads/person1.png")
img2 = Image.open("/Users/kirilvuchkov/Downloads/person2.jpg")

# print img1.size
# print img2.size

outdir = "/Users/kirilvuchkov/Desktop/imginterop"

loops = 10
images = list()
for i in range(loops+1):
    outfile = "%s/%02d.jpg" % (outdir, i)
    print(outfile)
    fac = 1.0/loops * i
    print(fac)
    frame = image_interop(img1, img2, fac)
    frame.save(outfile)
    images.append(frame)

gifOut = "%s/anim.gif" % (outdir)
writeGif("/Users/kirilvuchkov/Desktop/imginterop/ttt.gif", images, duration=0.2)