import sys
from os import path
from os import listdir
from PIL import Image
import numpy
import math

def norm(v):
    s = 0;
    for x in v:
        s += x*x
    ln = math.sqrt(s)

    for i in range(len(v)):
        v[i] = v[i]/ln
    return v

def encode(file):
    with open(file, "rb") as f:
        data = f.read()
    return data.encode("base64")

def plot_histogram(f_hist, images):
    colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'w']
    import matplotlib.pylab as plot
    plot.figure()
    plot.title('Histogram')
    plot.xlabel('Bins')
    plot.ylabel('Number of Pixels')
    for i in range(len(images)):
        plot.plot(norm(f_hist(images[i])), colors[i%len(colors)])
        # print f_hist(images[i])
    plot.xlim([0, 256])
    # plot.show()

def similarity_naive(v1, v2):
    dist = 0.0
    for i in range(len(v1)):
        dist += math.pow(v1[i]-v2[i], 2)
    return math.sqrt(dist)

def similarity_chisq(v1, v2):
    norm(v1)
    norm(v2)
    dist = 0.0
    for i in range(len(v1)):
        dist += 0 if (v1[i] + v2[i]) == 0 else ( math.pow(v1[i] - v2[i], 2) / (v1[i] + v2[i]) )/2
    return dist


def generage_result_html(query, pics):
    from UserString import MutableString
    html = MutableString()
    html.append("<html>")
    html.append("<head>")
    html.append("<style>")
    html.append(".sim-image {")
    html.append("display:inline-block;\nwidth:25%;margin-bottom:20px;")
    html.append("}")
    html.append("img {")
    html.append("width:90%;")
    html.append("}")
    html.append("</style>")
    html.append("</head>")
    html.append("<body>")
    html.append("<h1>Query Image<h1>")
    html.append("<div class=\"sim-image\">")
    html.append("<img src=\"data:image/jpeg;base64,")
    html.append(encode(query))
    html.append("\" />")
    html.append("</div>")
    html.append("<h1>Similar Images<h1>")
    for pic in pics:
        html.append("<div class=\"sim-image\">")
        html.append("<img src=\"data:image/jpeg;base64,")
        html.append(encode(pic[1]))
        html.append("\" />")
        html.append("</div>")
    html.append("</body>")
    html.append("</html>")
    return html

def image_by_index(libraryDir, i):
    files = listdir(libraryDir)
    fullPath = path.join(libraryDir, files[i])
    return Image.open(fullPath)

def find_similar(img, libraryDir, n, get_hist):
    # searchHist = img.convert('HSV').histogram()
    # searchHist =  img.histogram()
    searchHist = get_hist(img)
    best = list()
    for file in listdir(libraryDir):
        fullPath = path.join(libraryDir, file)
        libImage = Image.open(fullPath);
        # libImageHist = libImage.convert('HSV').histogram()
        # libImageHist = libImage.histogram()
        libImageHist = get_hist(libImage)
        currentSim = similarity_chisq(searchHist, libImageHist)
        best.append((currentSim, fullPath))
    best = sorted(best, key=lambda t: t[0])
    while len(best) > n:
        best.pop()

    return best

def sim_in_sobel(img1, img2):
    # sobel1 = sobel_hist_separate(img1)
    # sobel2 = sobel_hist_separate(img2)
    # return (similarity_chisq(sobel1[0], sobel2[0]) + similarity_chisq(sobel1[1], sobel2[1]))/2
    return similarity_chisq(sobel_hist(img1), sobel_hist(img2))

def sim_in_hist(img1, img2):
    fhist = rgb_hist
    return similarity_chisq(fhist(img1), fhist(img2))

def split(img):
    sx = img.size[0]
    sy = img.size[1]
    return [img.crop([0, 0, sx/2, sy/2]),
            img.crop([sx/2, 0, sx, sy/2]),
            img.crop([0, sy/2, sx/2, sy]),
            img.crop([sx/2, sy/2, sx, sy])]

def sim_exp(img1, img2, depth):
    # print "comparing depth %d" % (depth)
    score = (0.5 * sim_in_hist(img1, img2) + 0.5 * sim_in_sobel(img1, img2))
    if(depth == 0):
        return score
    # print "splitting %dx%d vs %dx%d" % (img1.size[0], img1.size[1], img2.size[0], img2.size[1])
    parts1 = split(img1)
    parts2 = split(img2)
    for i in range(len(parts1)):
        score += 0.25*sim_exp(parts1[i], parts2[i], depth-1)

    return score


def find_similar_2(img, libraryDir, n):
    best = list()
    for file in listdir(libraryDir):
        if file.startswith('.'):
            continue
        fullPath = path.join(libraryDir, file)
        libImage = Image.open(fullPath);
        currentSim = sim_exp(img, libImage, 1)
        best.append((currentSim, fullPath))
    best = sorted(best, key=lambda t: t[0])
    while len(best) > n:
        best.pop()
    return best

def rgb_hist(img):
    return img.histogram()

def hsv_hist(img):
    hist = img.convert('HSV').histogram()
    norm(hist)
    return hist

def sobel_hist_separate(img):
    from PIL import ImageFilter
    from PIL import ImageChops
    sobelx = [-1, 0, 1,
              -2, 0, 2,
              -1, 0, 1]

    sobely = [-1, -2, 1,
               0, 0, 0,
               1, 2, 1]
    img = img.convert("L")
    img = img.filter(ImageFilter.GaussianBlur(3))
    byy = img.filter(ImageFilter.Kernel((3, 3), sobely, scale=1))
    byx = img.filter(ImageFilter.Kernel((3, 3), sobelx, scale=1))
    # byx.show()
    # byy.show()
    return (byx.histogram(), byy.histogram())

def sobel_hist(img):
    from PIL import ImageFilter
    from PIL import ImageChops
    sobelx = [-1, 0, 1,
              -2, 0, 2,
              -1, 0, 1]

    sobely = [-1, -2, 1,
               0, 0, 0,
               1, 2, 1]
    img = img.convert("L")
    edged = img.filter(ImageFilter.GaussianBlur(2)).filter(ImageFilter.FIND_EDGES);
    edged = edged.point(lambda x: 0 if x < 10 else 255)
    # edged.show()
    return edged.histogram()
    img = img.filter(ImageFilter.GaussianBlur(4))
    byy = img.filter(ImageFilter.Kernel((3, 3), sobely, scale=1))
    byx = img.filter(ImageFilter.Kernel((3, 3), sobelx, scale=1))
    filtered = ImageChops.add(byx, byy)
    # filtered.show()
    return filtered.histogram()

def print_usage():
    print "Usage:\n$ python similars.py <path_to_the_image_library> <search_image> <number_of_result_images>"

if len(sys.argv) != 4:
    print_usage()
elif not(path.isdir(sys.argv[1])):
    print "The image library path is invalid"
    print_usage()
elif not(path.isfile(sys.argv[2])):
    print "The search image path is invalid"
    print_usage()
else:
    searchImage = Image.open(sys.argv[2])
    # plot_histogram(rgb_hist, searchImage)
    # libimg1 = image_by_index(sys.argv[1],3)
    # libimg = image_by_index(sys.argv[1],9)
    # libimg.show()
    # libimg1.show()
    # plot_histogram(sobel_hist, [searchImage, libimg])
    # print sim_in_sobel(searchImage, libimg)
    # print sim_in_sobel(searchImage, libimg1)
    # libimg.show()
    # print sim_exp(searchImage, libimg, 2)
    # split(libimg)[0].show()
    # split(libimg)[3].show()
    # searchImage.show()
    # searchImage.crop([0,0,100,100]).show();
    # searchImage.resize([100, 100]).show();
    # libimg.show();
    # plot_histogram(hsv_hist, [searchImage, libimg])
    # sobel_hist(searchImage)

    strange = Image.open('/Users/kirilvuchkov/Desktop/db/tamarindo-barcelo-hotels-beach54-3908.jpg')
    # strange.show()
    # plot_histogram(sobel_hist, [searchImage, strange])
    # plot_histogram(rgb_hist, [searchImage, strange])
    # print sim_in_hist(searchImage, strange)
    # print sim_in_sobel(searchImage, strange)
    # strange.show()

    libraryDir = sys.argv[1]
    # related = find_similar(searchImage, libraryDir, int(sys.argv[3]), rgb_hist)
    related = find_similar_2(searchImage, libraryDir, int(sys.argv[3]))
    print generage_result_html(sys.argv[2], related)

    # forplot = list()
    # forplot.append(searchImage)
    # for pic in related:
    #     forplot.append(Image.open(pic[1]))
    # plot_histogram(rgb_hist,forplot)

